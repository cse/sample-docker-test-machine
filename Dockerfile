FROM alpine:latest

COPY to-docker-image/* .

# Add necessary packages
# List of packages: https://pkgs.alpinelinux.org/packages
# How to use `apk`: https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management
RUN apk update && apk upgrade && apk add git make

RUN chmod 700 /*.sh

