# Sample docker image to test assignments

This machine can be modified or extended to run your tests

# If you want to modify it

* Change the machine name in `MACHINE_NAME` file
* The contents of `to-docker` directory will be copied to the virtual machine
  root filesystem
* Edit `Dockerfile` if necessary 
* Try to make images minimalistic but reusable e.g. include the necessary tools
  (compilers, linters, analyzers etc.) but not the tests for each assignment
   you are expecting to test using the image
